# EyeQ
A web-app for improving eye sight and it suggests eye exercises while using mobile phones, laptops and watching TVs. 

* This project was created in BorderHacks'21

# What is the purpose of this project?#

1. The purpose of this project is to make a web application so that people can test their eyes in it, if they are confused about their vision.

2. It suggests eye exercise for improving eye sights.

3. Its created on the theme ' Healthcare'. Borderhacks was conducted virtually where students from all over the world participated with great zeal. 

* TECH STACK and IDE : HTML, CSS, Vanilla JS, VScode

Demo video: https://www.youtube.com/watch?time_continue=22&v=65mvYoz_TI4&feature=emb_logo

Devpost: https://devpost.com/software/eyeq-eye-quality


